# Binar Car Rental

    Website ini dibuat mengunakan HTML, Javascript(Boostrap) dan juga Css. dibuat agar bisa respoonsive dengan mode Mobile dan Desktop.


## File, Folder, Icon, Image
    
    asset berisi :
        •	css("style.css", "flickity.css", "boostrap", dan 2 icon slider testimonial)
        •	image(footer, Our Service, testi, why us, dan beberapa icon)
        •	js

    index.html bersisi sorce code yang dapat diakases dan ditampilkan melalui browser internet.
    
## Framework

    Bootstrap yang pakai versi 4 dan 5, bootstrap versi 4 dipakai untuk penambahan class pada setiap elemen yang ada di html, dan bootstrap 5 dipakai untuk menambah offcanvas yang ditampilkan pada saat diakses dari mode mobile. boostrap ini menggunakan cdn yang diakses secara online sehingga tidak ada yang harus diunduh untuk menggunakan tampilan yang ada di dalam website ini.

    AOS(Animate On Scroll) diakses dari github digunakan untuk menampilkan animasi pada beberapa container sehingga tampilan website menjadi lebih dinamis.

    Flickity dipakai untuk menampilkan carousel dan slider pada bagian testimonial yang sudah dicustom beberapa elemen seperti button dan juga tampilannya dari warna dan properti lainnya pada file flickity.css dan juga style.css

## Open and Run

    •	Buka aplikasi Visual Studio Code di komputer atau laptop.

    •	Klik “file” yang ada di tools – Open folder – pilih file folder yang berisi file data yang ingin dijalankan – Klik “select folder”.

    •	Kemudian buka folder tersebut di VScode, – Klik file yang ingin dibuka contohnya “index.html”.

    •	Running Program web
        1.	Cara pertama buka file “index.html” – Klik “Run” pada bagian tools – Pilih “Run Without Debuging” – pilih browsernya – web terbuka.
        2.	Cara kedua buka file “index.html” – Klik kanan pada mouse – Klik “Open with live server” – web terbuka (sebelumnya harus sudah menambahkan Ekstension Live Server di VScode).

    •	Jika ingin melihat web tersebut bisa dijalankan disemua prangkat (Responsive) atau tidak, Klik kanan pada mouse – Klik inspect – silahkan atur di “Dimensions”.